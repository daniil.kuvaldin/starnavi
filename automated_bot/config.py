import os
from dotenv import load_dotenv


class Config(object):
    load_dotenv()

    NUMBER_OF_USERS = os.getenv("NUMBER_OF_USERS")
    MAX_POSTS_PER_USER = os.getenv("MAX_POSTS_PER_USER")
    MAX_LIKES_PER_USER = os.getenv("MAX_LIKES_PER_USER")
    HOST_URL = os.getenv("HOST_URL")
    STRATEGY = os.getenv("STRATEGY")
    SIGNUP_LINK = f"{HOST_URL}/api/user/signup/"
    POST_LINK = f"{HOST_URL}/api/post/"
    LIKE_LINK = f"{HOST_URL}/api/post/#/like/"
    LOGIN_LINK = f"{HOST_URL}/api/user/login/"

