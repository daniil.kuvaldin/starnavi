import sys

from config import Config
from bot import Bot

if __name__ == '__main__':
    strategy = None
    if len(sys.argv) > 1:
        if sys.argv[1].isdigit():
            strategy = int(sys.argv[1])
    print("Be sure that your .env file is fulfilled")
    bot_object = Bot(Config, strategy=strategy)
    bot_object.start()
