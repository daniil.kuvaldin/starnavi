from typing import List

from bot.models.user import User


class BotStorage:
    def __init__(self):
        self._users: List[User] = []
        self._likes = []
        self._posts = []

    def get_all_users(self) -> list:
        return self._users

    def get_all_likes(self) -> list:
        return self._likes

    def get_all_posts(self) -> list:
        return self._posts

    def update_users(self, updated_users: list) -> None:
        self._users = updated_users

    def update_likes(self, updated_likes: list) -> None:
        self._likes = updated_likes

    def update_posts(self, updated_posts: list) -> None:
        self._posts = updated_posts
