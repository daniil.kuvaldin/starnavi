import random
import string

from typing import Type, List


__all__ = ["Randomizer"]


class Randomizer:
    @staticmethod
    def get_random_string(length: int = 10) -> str:
        letters = string.ascii_lowercase
        result_str = ''.join(random.choice(letters) for i in range(length))
        return result_str

    @staticmethod
    def get_random_int(maximum: int) -> int:
        return random.randint(1, maximum)
