import ujson

from typing import Union
from abc import ABC, abstractmethod
from bot.utils import Randomizer
from enum import IntEnum


__all__ = ["RandomStrategy", "IndexedStrategy", "StrategyDataTypes", "FromFileBotDataStrategy", "RandomIndexedBotDataStrategy"]


class StrategyDataTypes(IntEnum):
    INT = 1
    STR = 2


class Strategy(ABC):
    def __init__(self, datatype: StrategyDataTypes):
        self._storage = {}
        self._datatype = datatype

    def _is_exists(self, field: str):
        if field not in self._storage:
            self._storage[field] = set()

    @abstractmethod
    def get_strategy_field(self, field: str, **kwargs):
        pass


class RandomStrategy(Strategy):
    def get_strategy_field(self, field: str, **kwargs):
        self._is_exists(field)
        generated_field = (
            Randomizer.get_random_string() if self._datatype == StrategyDataTypes.STR else
            Randomizer.get_random_int(kwargs.get("maximum_int", 10))
        )
        while generated_field in self._storage[field]:
            generated_field = (
                Randomizer.get_random_string() if self._datatype == StrategyDataTypes.STR else
                Randomizer.get_random_int(kwargs.get("maximum_int", 10))
            )
        self._storage[field].add(generated_field)
        return generated_field


class IndexedStrategy(Strategy):
    def get_strategy_field(self, field: str, **kwargs):
        self._is_exists(field)
        if self._datatype == StrategyDataTypes.STR:
            result = f"{field}_{len(self._storage[field])}"
            self._storage[field].add(result)
            return result
        else:
            result = len(self._storage[field])
            self._storage[field].add(result)
            return result


class BotDataStrategy(ABC):
    @abstractmethod
    def get_user_data(self):
        pass

    @abstractmethod
    def get_post_data(self):
        pass


class FromFileBotDataStrategy(BotDataStrategy):
    def __init__(self, file_path: str):
        print(file_path)
        with open(file_path, "r") as json_file:
            data = ujson.load(json_file)
            self.posts: list = data.get("posts")
            self.users: list = data.get("users")

    def get_user_data(self):
        for user in self.users:
            yield user

    def get_post_data(self):
        for post in self.posts:
            yield post


class RandomIndexedBotDataStrategy(BotDataStrategy):
    def __init__(
        self,
        is_random: bool,
        # 1 -- random, 2 -- indexed
        max_posts: int,
        max_likes: int,
        max_users: int,
    ):
        self.strategy_str = RandomStrategy(StrategyDataTypes.STR) if is_random else IndexedStrategy(StrategyDataTypes.STR)
        self.strategy_int = RandomStrategy(StrategyDataTypes.INT) if is_random else IndexedStrategy(StrategyDataTypes.INT)
        self.number_of_posts = self.strategy_int.get_strategy_field(
            f"post_num",
            maximum_int=max_posts
        )
        self.number_of_likes = self.strategy_int.get_strategy_field(
            f"likes_num",
            maximum_int=max_likes
        )
        self.number_of_users = max_users

    def get_user_data(self):
        for i in range(self.number_of_users):
            username = self.strategy_str.get_strategy_field("username")
            email = self.strategy_str.get_strategy_field("email") + "@bot.ru"
            password = self.strategy_str.get_strategy_field("password")
            yield {
                "username": username,
                "email": email,
                "password": password
            }

    def get_post_data(self):
        for i in range(self.number_of_posts):
            yield {
                "title": f"BotTitle \n{self.strategy_str.get_strategy_field('title')}",
                "body": f"BotBody \n{self.strategy_str.get_strategy_field('post')}",
            }


def choose_strategy(
        strategy: int = 1,
        # 1 -- random, 2 -- indexed
        max_posts: int = 10,
        max_likes: int = 10,
        max_users: int = 10,
        file_path: str = "data.json",
) -> Union[RandomIndexedBotDataStrategy, FromFileBotDataStrategy]:
    if strategy == 1 or strategy == 2:
        return RandomIndexedBotDataStrategy(
            True if strategy == 1 else False,
            max_posts,
            max_likes,
            max_users
        )
    elif strategy == 3:
        return FromFileBotDataStrategy(file_path=file_path)
