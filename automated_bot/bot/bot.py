import random

from bot.utils.storage import BotStorage
from bot.apiclient import BotAPIClient
from config import Config

from bot.models.user import User
from bot.utils.strategy import choose_strategy


class Bot:
    def __init__(self, config: Config, **kwargs):
        self.storage: BotStorage = BotStorage()
        self.bot_api = BotAPIClient(
            config.LIKE_LINK,
            config.POST_LINK,
            config.LOGIN_LINK,
            config.SIGNUP_LINK,
        )

        self.target_number_of_users = int(config.NUMBER_OF_USERS)
        self.max_likes_per_user = int(config.MAX_POSTS_PER_USER)
        self.max_posts_per_user = int(config.MAX_LIKES_PER_USER)
        strategy = int(config.STRATEGY)
        strategy = kwargs.get("strategy", strategy)
        self.strategy = choose_strategy(
            strategy,
            self.max_posts_per_user,
            self.max_likes_per_user,
            self.target_number_of_users
        )
        if (
            not self.target_number_of_users or
            not self.max_posts_per_user or
            not self.max_likes_per_user
        ):
            raise RuntimeError("There are no data in .env file")

    def _create_posts_per_user(self, user: User) -> list:
        posts = []
        for data in self.strategy.get_post_data():
            new_post = self.bot_api.create_post(user, data)
            if new_post:
                posts.append(new_post)
        return posts

    def _create_users(self):
        users = []
        for data in self.strategy.get_user_data():
            new_user = self.bot_api.signup_user(
                **data
            )
            if not new_user:
                continue
            new_user = self.bot_api.proceed_token(new_user)
            users.append(new_user)
        self.storage.update_users(updated_users=users)

    def _create_posts(self):
        users = self.storage.get_all_users()
        all_posts = []

        for user in users:
            posts = self._create_posts_per_user(user)
            all_posts.extend(posts)

        self.storage.update_users(users)
        self.storage.update_posts(all_posts)

    def _spread_likes(self):
        all_posts = self.storage.get_all_posts()
        all_users = self.storage.get_all_users()
        all_likes = []

        for user in all_users:
            number_of_likes = random.randint(1, self.max_likes_per_user)
            likes = []
            for i in range(number_of_likes):
                post = random.choice(all_posts)
                like = self.bot_api.proceed_like_to_post(post, user)
                if like:
                    likes.append(like)
            all_likes.extend(likes)

        self.storage.update_users(all_users)
        self.storage.update_likes(all_likes)

    def start(self):
        print("Starting bot...")
        self._create_users()
        print(f"{len(self.storage.get_all_users())} users was created")
        self._create_posts()
        print(f"{len(self.storage.get_all_posts())} posts was made")
        self._spread_likes()
        print(f"{len(self.storage.get_all_likes())} likes was made")
        print("Bot finished his work")
