__all__ = ["User"]


class User:
    def __init__(self, user_dict=None):
        if user_dict is None:
            user_dict = {}
        self._email: str = user_dict.get("email")
        self._username: str = user_dict.get("username")
        self._password: str = user_dict.get("password")
        self._token: str = user_dict.get("token")

    def get_login_cred(self):
        return {
            "email": self.email,
            "password": self.password
        }

    def get_signup_cred(self):
        return {
            "email": self.email,
            "username": self.username,
            "password": self.password
        }

    @property
    def email(self):
        return self._email

    @property
    def username(self):
        return self._username

    @property
    def password(self):
        return self._password

    @property
    def token(self):
        return self._token

    @token.setter
    def token(self, value):
        self._token = value

    @email.setter
    def email(self, value):
        self._email = value

    @password.setter
    def password(self, value):
        self._password = value

    @username.setter
    def username(self, value):
        self._username = value