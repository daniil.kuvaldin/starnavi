import requests
from typing import Optional

from http import HTTPStatus
from bot.utils.randomizer import Randomizer
from config import Config
from bot.models.user import User

__all__ = ["BotAPIClient"]


class BotAPIClient:
    def __init__(
            self,
            like_link: str,
            post_link: str,
            login_link: str,
            signup_link: str,
    ):
        self.like_link = like_link
        self.post_link = post_link
        self.login_link = login_link
        self.signup_link = signup_link

    @staticmethod
    def _make_request(link: str, data: dict = None, method: str = "GET", headers: dict = None) -> Optional[dict]:
        response = requests.request(
            method=method,
            url=link,
            data=data,
            headers=headers
        )

        if (response.status_code == HTTPStatus.OK or
                response.status_code == HTTPStatus.CREATED):
            return response.json()

    @staticmethod
    def _get_headers(user: User) -> dict:
        return {
            "Authorization": f"Bearer {user.token}"
        }

    def proceed_token(self, user: User) -> User:
        data = user.get_login_cred()
        parsed_data = self._make_request(
            link=self.login_link,
            data=data,
            method="POST"
        )
        user.token = parsed_data.get("token")
        return user

    def create_post(self, user: User, data: dict) -> dict:
        headers = self._get_headers(user)

        return self._make_request(
            link=self.post_link,
            data=data,
            headers=headers,
            method="POST"
        )

    def proceed_like_to_post(self, post: dict, user: User) -> dict:
        headers = self._get_headers(user)
        data = {
            "is_like": True,
        }
        return self._make_request(
            link=self.like_link.replace("#", f"{post.get('id')}"),
            data=data,
            headers=headers,
            method="POST"
        )

    def signup_user(
            self,
            email: str,
            password: str,
            username: str,
    ) -> Optional[User]:
        future_user = User()
        future_user.email = email
        future_user.password = password
        future_user.username = username

        response = self._make_request(
            link=self.signup_link,
            data=future_user.get_signup_cred(),
            method="POST"
        )

        if response:
            return future_user
