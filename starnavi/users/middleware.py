from django.utils import timezone
from django.contrib.auth import authenticate, login


class LastActivityMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if not request.user.is_authenticated:
            if user := authenticate(request):
                login(request, user)

        if request.user.is_authenticated:
            request.user.last_activity = timezone.now()
            request.user.save(update_fields=['last_activity'])

        return self.get_response(request)
