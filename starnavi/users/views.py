from rest_framework import permissions, viewsets

from .serializers import UserRegisterSerializer, UserActivitySerializer
from .models import User


class UserViewSet(viewsets.ModelViewSet):
    serializer_class = UserRegisterSerializer
    permission_classes = (permissions.AllowAny,)


class UserActivity(viewsets.ModelViewSet):
    serializer_class = UserActivitySerializer
    queryset = User.objects.all()
