from django.contrib.auth.backends import BaseBackend

from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from .models import User


class JWTAuthBackend(BaseBackend):
    """
    Authenticate against the settings ADMIN_LOGIN and ADMIN_PASSWORD.
    Use the login name and a hash of the password
    """

    def authenticate(self, request, **kwargs):
        try:
            user, payload = JSONWebTokenAuthentication().authenticate(request)
        except:
            user, payload = None, None
        return user

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
