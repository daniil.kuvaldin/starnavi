from django.urls import path, include
from rest_framework_jwt import views as jwt_views

from .views import UserViewSet, UserActivity

user_urls = [
    path("login/", jwt_views.ObtainJSONWebToken.as_view(), name="token-obtain"),
    path("signup/", UserViewSet.as_view({"post": "create"}), name="account-signup"),
    path("activity/", UserActivity.as_view({"get": "list", "retrieve": "retrieve"}), name="account-activity"),

]

urlpatterns = [
    path('user/', include(user_urls)),
]
