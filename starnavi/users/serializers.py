from rest_framework import serializers

from .models import User


class UserRegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["email", "username", "password", "first_name", "last_name"]
        extra_kwarg = {
            "email": {"required": True},
            "username": {"required": True},
            "password": {
                "required": True,
                "write_only": True
                # Do not working, spend a lot of time make password "write_only"...
            }
        }

    def create(self, validated_data):
        user = User.objects.create_user(
            **validated_data
        )
        user.password = "hashed"
        # Maybe bad decision, but anyway...
        return user


class UserActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("last_login", "last_activity", "id", "username")
        read_only_fields = fields
