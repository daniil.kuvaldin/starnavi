from django.db import models


class Post(models.Model):
    """User's post model"""
    # Fields
    title = models.CharField(max_length=100)
    body = models.CharField(max_length=300)
    created_at = models.DateTimeField(auto_now_add=True)

    # Relations
    owner = models.ForeignKey(to='users.User', on_delete=models.CASCADE)

    class Meta:
        db_table = 'posts'


class Like(models.Model):
    """User's like to post"""
    # Fields
    is_like = models.BooleanField(verbose_name="status for this like activity", default=True)
    last_edited = models.DateTimeField(auto_now=True, db_index=True)

    # Relations
    owner = models.ForeignKey(to='users.User', on_delete=models.CASCADE)
    post = models.ForeignKey(to='posts.Post', on_delete=models.CASCADE)

    @property
    def status(self):
        return "like" if self.is_like else "unlike"

    class Meta:
        ordering = ["last_edited"]
