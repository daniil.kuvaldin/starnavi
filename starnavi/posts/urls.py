from django.urls import path, include

from .views import PostViewSet, LikeViewSet, AnalyticsViewSet


posts_urls = [
    path("", PostViewSet.as_view({"post": "create", "get": "list"})),
    path("<int:post_pk>/like/", LikeViewSet.as_view({"post": "create"})),
    path("likes/", LikeViewSet.as_view({"get": "list"})),
    path("analytics/", AnalyticsViewSet.as_view({"get": "get"}))

]

urlpatterns = [
    path("post/", include(posts_urls))
]
