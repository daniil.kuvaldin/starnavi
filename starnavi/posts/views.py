import pytz
from datetime import datetime
from django.db.models import Count, Min, Max
from rest_framework import viewsets, status
from rest_framework.response import Response

from .serializers import PostCreationSerializer, PostSerializer, LikeCreationSerializer, LikeSerializer
from .models import Post, Like


class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = None

    def get_serializer_class(self):
        return PostCreationSerializer if self.action == "create" else PostSerializer


class LikeViewSet(viewsets.ModelViewSet):
    queryset = Like.objects.all()
    serializer_class = LikeSerializer

    def get_serializer_class(self):
        return LikeCreationSerializer if self.action == "create" else LikeSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data={
            "post": self.kwargs.get("post_pk"),
            "is_like": request.data.get("is_like")
        })
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class AnalyticsViewSet(viewsets.ViewSet):
    _start_date = None
    _finish_date = None

    def parse_query_parameters(self, request):
        date_to = request.query_params.get("date_to")
        date_from = request.query_params.get("date_from")

        min_date = Like.objects.aggregate(Min("last_edited")).get("last_edited__min")
        max_date = Like.objects.aggregate(Max("last_edited")).get("last_edited__max")

        utc = pytz.UTC

        if date_from:
            self._start_date = min(
                utc.localize(datetime.strptime(date_from, "%Y-%m-%d")),
                min_date
            )
        else:
            self._start_date = min_date

        if date_to:
            self._finish_date = max(
                utc.localize(datetime.strptime(date_to, "%Y-%m-%d")),
                max_date
            )
        else:
            self._finish_date = max_date

    def get(self, request):
        self.parse_query_parameters(request)
        metrics = {
            'total': Count('last_edited__date'),
        }

        return Response(
            Like.objects.all()
                .filter(last_edited__gte=self._start_date, last_edited__lte=self._finish_date)
                .values('last_edited__date')
                .annotate(**metrics)
                .order_by('last_edited__date')
        )
        # Similar to:
        # SELECT
        #     django_datetime_cast_date("posts_like"."last_edited", 'UTC', 'UTC'),
        #     COUNT("posts_like"."last_edited") AS "total"
        # FROM
        #     "posts_like"
        # WHERE
        #     ("posts_like"."last_edited" >= 2021 - 03 - 18 00: 00:00
        # AND
        #     "posts_like"."last_edited" <= 2021 - 03 - 24 02: 23:49.584399)
        # GROUP BY
        #     django_datetime_cast_date("posts_like"."last_edited", 'UTC', 'UTC')
        # ORDER BY
        #     django_datetime_cast_date("posts_like"."last_edited", 'UTC', 'UTC') ASC
