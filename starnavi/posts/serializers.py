from rest_framework import serializers

from .models import Post, Like


class PostSerializer(serializers.ModelSerializer):
    owner = serializers.CharField(source="owner.username")

    class Meta:
        model = Post
        fields = "__all__"


class PostCreationSerializer(serializers.ModelSerializer):
    owner = serializers.HiddenField(
        default=serializers.CurrentUserDefault(),
    )

    class Meta:
        model = Post
        fields = ["title", "body", "owner", "id"]
        read_only_fields = ["id"]


class LikeSerializer(serializers.ModelSerializer):
    owner = serializers.CharField(source="owner.username")
    post = serializers.CharField(source="post.title")

    class Meta:
        model = Like
        fields = "__all__"


class LikeCreationSerializer(serializers.ModelSerializer):
    owner = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )

    def create(self, validated_data):
        like, created = Like.objects.update_or_create(
            owner=validated_data.get("owner"),
            post=validated_data.get("post"),
            defaults={'is_like': validated_data.get('is_like')}
        )
        return like

    class Meta:
        model = Like
        fields = ["is_like", "owner", "post"]
        extra_kwargs = {
            "is_like": {"required": True}
        }
